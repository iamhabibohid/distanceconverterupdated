package com.mainor.distanceconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    String MESSAGE;

    final DecimalFormat df = new DecimalFormat("0.00");


    public String convertKmToMiles(double value){
        double r = (value * 0.62173);
        if (r>1){
            MESSAGE = "Miles";
        }else{
            MESSAGE = "Mile";
        }

        String result = df.format(r);
        return result;
    }

    public String convertMilesToKm(double value){
        double r = (value * 1.62173);
        if (r>1){
            MESSAGE = "Kilometers";
        }else{
            MESSAGE = "Kilometer";
        }
        String result = df.format(r);
        return result;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText distanceInputKM = findViewById(R.id.distanceInputKM);
        final EditText distanceInputMiles = findViewById(R.id.distanceInputMiles);
        final TextView displayResult = findViewById(R.id.displayResult);


        //KmToMiles
        distanceInputKM.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0){
                    double value = Double.parseDouble(s.toString());
                    displayResult.setText(convertKmToMiles(value) + MESSAGE);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //MilesToKm
        distanceInputMiles.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0){
                    double value = Double.parseDouble(s.toString());
                    displayResult.setText(convertMilesToKm(value) + MESSAGE);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
}
